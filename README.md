# chapter_reader IDEA 在线&本地阅读插件


# 公告信息：
最新公告：新版本已经提交审核

有任何想法和建议，QQ群私聊我或者下方语雀直接编辑。
邀请你共同编辑文档《chapter_reader 意见收集》
https://www.yuque.com/g/wind./erkbzf/wzeszu39c0toiyby/collaborator/join?token=x7dTAvRFQ3RR5ZTL


# 文章中所有用【】括起来，仅为侧重表达的意思，无其它含义。

# 介绍
✅ 历史记录自动保存 ✅ 自定义适配想看的网站

✅ 在线章节阅读 - gitee配置文件中已配置的网站

✅ 本地txt章节阅读 ✅ 本地epub章节阅读

✅ 本地txt简易阅读 ✅ 本地txt换行符阅读
```
"chapter_reader" 是一款IDEA 在线&本地 小说阅读插件
为了帮助各位道友在资本主义的魔爪下，有一个可以调节情绪且可以畅游知识海洋的环境。
"chapter_reader"应运而生。只希望可以帮助更多的人。
(请勿痴迷，该学习则学习，该放松则放松，让加班？不可能！)
希望大家看到这篇文章可以，点赞评论，让更多人知道即可。

遇到问题及时加QQ群，各位道友都会热心帮助的。
插件：chapter_reader
作者：wind
邮件：wind_develop@163.com
QQ群：1群：252192636，2群：251897630（有疑问会有各位道友或作者在线教学）

gitee：https://gitee.com/wind_invade/chapter_reader
博客园：https://www.cnblogs.com/winddniw/p/15689784.html
git: https://windinvade.github.io/chapter_reader/
```

# 安装方式

```
1. IDEA插件市场直接安装：File | Settings | Plugins | 搜索chapter_reader

2. 通过idea插件网下载安装：https://plugins.jetbrains.com/plugin/16544-chapter-reader
```

# 版本更新说明（推荐及时升级最新版）IDEA审核需要2-3个工作日。
- **及时更新最新版本，获得优质体验**
- **遇见无法阅读的BUG，及时私聊与我联系，修复包会先发在群里**
```
- v3.0.0
    1. 重新调整代码，为后续【插中插做准备】（在此插件上再插一回）
    2. 优化跨章节、章节跳转时，无需手动下一页。
    3. 优化翻页快捷键，原本普通阅读和全章节快捷键不通用，现在统一通过【上/下页】快捷键操作。
    4. 修复代码弹出框阅读跨章报错
    5. 新增epub全章节阅读
    6. 导航栏阅读模式已支持新版UI，注：点击类的层级过深，会导致无法正常显示。已尽力破坏导航栏结构。
    7. 新增在线阅读超时配置
```

# 使用说明
[使用说明文档](https://gitee.com/wind_invade/chapter_reader/blob/master/%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E.MD)

# 在线阅读配置说明
[在线阅读配置说明](https://gitee.com/wind_invade/chapter_reader/blob/master/%E5%9C%A8%E7%BA%BF%E9%98%85%E8%AF%BB%E7%BD%91%E7%AB%99%E9%80%82%E9%85%8D.MD)

# 版本号说明（idea插件市场搜索时，会自动显示匹配当前版本）：

## 【x.x.x 如：2.1.5】版本（低版本IDEA）

![在这里插入图片描述](https://img-blog.csdnimg.cn/258acf99e5cc4e8e8ae705184c8f29c2.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)

## 【x.x.x-high 如：2.1.5-high】版本（高版本IDEA）

![在这里插入图片描述](https://img-blog.csdnimg.cn/ad6cf0146dd445a6b6170057df639335.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)


# 快捷键设置方法
```
IDEA | File | Settings | Keymap | Plug-ins | 找到chapter_reader 详细看下图
翻页推荐快捷键，其它自行设置：
- 小键盘"+" "-"(目前最轻松的体验方式)
- 鼠标多功能按键
- 按键 + 鼠标滚轮 (必须是按键+鼠标滚轮, 否则会因优先级的问题, 无法生效)
- 组合快捷键(会很累)
```
![](https://img-blog.csdnimg.cn/7815fa6ee40642e1a2570d39b03453cb.png)



# 如出现右下角有弹窗提示，操作步骤
```
File | Settings | Appearance & Behavior | Notifications | 搜索novel  | 将Popup 修改为 No popup
详细看下图
```
![](https://img-blog.csdnimg.cn/de8fa52272cf4436be670ca55e83298d.png)


# 4. 截图演示

## 菜单展示

![在这里插入图片描述](https://img-blog.csdnimg.cn/3b92fb7dddb84418ad617c083a25f57f.png)

## 设置菜单
![在这里插入图片描述](https://img-blog.csdnimg.cn/57af510e7be54de9a605defae56988e3.png)

## 左下角展示（需将鼠标焦点触发在IDEA内部窗口中）

![在这里插入图片描述](https://img-blog.csdnimg.cn/a00059a3ef01464daa3b907c08e412c2.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)

## 弹出框展示（需将鼠标焦点放在文件内）

![在这里插入图片描述](https://img-blog.csdnimg.cn/d0867a94033f4b83b14df7c29dd9f226.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)

## 导航栏展示

![在这里插入图片描述](https://img-blog.csdnimg.cn/a1b1de7e6b14474cab40af34fd403c96.png)

## 全章节阅读（右侧窗口，整章阅读，隐蔽性低）
双击鼠标左键隐藏上方按钮

双击鼠标左键并保持不松，拖动一下鼠标即可显示

翻页需点击，后续版本会新增：左← 右→方向键为快捷键
![在这里插入图片描述](https://img-blog.csdnimg.cn/0c0cb95fe159479e8f8f6e33d369fb3e.png)


# 5.特此声明（侵权免责声明）

```
无任何商业用途，无任何侵权想法。但如发现侵权或其它问题请 及时 且 成功 与作者本人取得联系。
作者本人会在第一时间进行相关内容的删除。

插件：chapter_reader
作者：wind
邮件：wind_develop@163.com
QQ群：252192636（有疑问会有各位道友或作者在线教学）
博客园：https://www.cnblogs.com/winddniw/p/15689784.html
csdn： https://blog.csdn.net/com_study/article/details/115709454（吃相难看，会渐渐放弃）

在这祝大家工作开心^_^
```

# 赞助一下~ 鼓励持续更新~

<div style="display:flex;">
    <img src="https://img-blog.csdnimg.cn/20210713161400215.png" width="49%" height="600px"><img src="https://img-blog.csdnimg.cn/20210713162109319.jpeg"   width="49%" height="450px">
</div>

